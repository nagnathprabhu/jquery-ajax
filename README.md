# About this repo #

This application fetches data from a JSON file, stores it to local storage and then parses the local storage to display the data in 
a tabular format. 

### Environment ###

* HTML 5
* JavaScript
* JQuery
* AJAX to make asynchronus requests.

### How do I get set up? ###

- Clone the repository
- Navigate to the directory containing the project
- Open a command window here and type `npm install http-server`. This will install the http server
- To launch the application type `http-server -c -1`

