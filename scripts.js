var data = JSON.parse(localStorage.getItem('data'))
if (data == null || data.length == 0) {
    var promise = new Promise(function (resovle, reject) {
        $.ajax({
            type: 'get',
            url: 'data.json',
            success: function (data) {
                resovle(data);
            }
        })
    }).then(function (data) {
        localStorage.setItem('data', JSON.stringify(data));
        drawTable();
    })
} else {
    drawTable();
}

function drawTable() {
    if ($('#tab')) {
        $('#tab').remove();
    }
    $('body').append("<table id='tab' border='1'><tr id = 'head'><th class='un'>Username</th><th class='age'>Age</th><th class='name'>Name</th><th class='gender'>Gender</th><th class='company'>Company</th><th class='phone'>Phone</th><th colspan ='2'>Options</th></tr></table>")
    data = JSON.parse(localStorage.getItem('data'));
    var len = data.length;
    for (let i = 0; i < len; i++) {
        if (data[i] !== null)
            $('#tab').append('<tr id=' + i + '><td class="username">' + data[i].username + '</td><td class="age">' + data[i].age + '</td><td class="name">' + data[i].name + '</td><td class="gender">' + data[i].gender + '</td><td class="company">' + data[i].company + '</td><td class="phone">' + 
            data[i].phone + 
            '</td><td><button id=\"view' + i + '\" >View</button></td><td><button id=\"dlte' + i + '\">Delete</button></td></tr>')
        }
    
    button_listener();
}

//button_listener();
//triggers the event only once



$(':checkbox').click(function (event) {
    var check = event.target.id;
    if (this.checked) {
        $("." + check).hide();
    } else {
        $("." + check).show();
    }
})


function getFriends(val) {
    var frnd = data[val].friends;
    var friends = "";
    var last = frnd.length - 1;

    for (var i = 0; i < frnd.length; i++) {
        if (i == last) {
            friends += frnd[i].name + '.';
        }
        else {
            friends += frnd[i].name + ", ";

        }
    }

    return friends;
}

function button_listener() {
    $('button').click(function (e) {
        var target_id = e.target.id
        var i = parseInt(target_id.substring(4));

        if (target_id.substring(0, 4) == "view") {
            if ($('.view' + i).length > 0) {
             $('.view' + i).toggle();
            
             } else {
           
                $("#" + i).after("<tr class='view" + i + " view'><td colspan ='6'><b>Balance:</b>" + data[i].balance + '<br>' +
                    '<b>Eye Color:</b>' + data[i].eyeColor + '<br><b>Email:</b>' + data[i].email + '<br><b>Address:</b>' + data[i].address +
                    '<br><b>Tags:</b>' + data[i].tags + '<br><b>Friends:</b>' + getFriends(i) + '<br><b>Picture:</b><img src=' + data[i].picture + "></td></tr>")

            }

           
        } else if (target_id.substring(0, 4) == "dlte") {
            if ($('.view' + i)) {
                $('.view' + i).remove();
            }
            $("#" + i).remove();
            data.splice(i, 1);
            localStorage.setItem('data', JSON.stringify(data));
            drawTable();
        }
    })

}



$('#search').keyup(function () {
    var value = this.value.toLowerCase();//convert the input to lowercase

    //iterate through each row and then each col. Save the col result in a var and see if it matches the input
    //If yes display
    $("table").find("tr").each(function (index) {
      if (index === 0) return;
        var result = $(this).find("td").text();
        $(this).toggle(result.toLowerCase().indexOf(value) !== -1);
    });
})